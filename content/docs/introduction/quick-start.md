---
weight: 1
title: Quick Start
bookToc: false
---

# Ready to get started?

You can follow the Get Started guide, or follow instructions below.

<div style="height: 30px" align="center">
<a class="button" href="{{< relref "/get-started" >}}">Get Started</a>
</div>

&nbsp;


## Quick Start

1. [Install Patchwork](http://dinosaur.is/patchwork-downloader/).
2. Boot it up and set up your profile.
3. Go to [the list of pub servers](https://github.com/ssbc/ssb-server/wiki/Pub-Servers) and get an invite code from one.
4. Click "+ Join Pub" in the top left corner of Patchwork and paste in the invite code.
5. Explore by browsing public posts and channels, expand into your extended network (Click on More > Extended Network).
6. Introduce yourself on the #new-people channel.
7. Follow people you find interesting. You will download content authored by everyone you follow, and everyone who they follow.
8. Have fun, and be respectful of others. The scuttleverse is a nice place.
